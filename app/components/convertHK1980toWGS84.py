from turtle import position
import requests
import numpy
import time
import datetime
import os
import json
from lxml import etree
import re

headers = {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0",
    "Connection": "close"
}


def convertHK1980toWGS84(coords):
    """
    coords(list): value of [easting, northing]
    """

    # http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&outSys=wgsgeog&e=832591.320&n=820359.389

    if coords[0] == 0 and coords[1] == 0:
        finalCoords = {
            "WSG84": {
                "lat": coords[0],
                "lng": coords[0]
            },
            "EPSG2326": {
                "easting": coords[0],
                "northing": coords[1]
            }
        }
    else:
        try:
            res = requests.get("http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&outSys=wgsgeog&e={}0&n={}".format(
                coords[0], coords[1]), headers=headers).json()

            finalCoords = {
                "WSG84": {
                    "lat": res["wgsLat"],
                    "lng": res["wgsLong"]
                },
                "EPSG2326": {
                    "easting": coords[0],
                    "northing": coords[1]
                }
            }
        except:
            print("Cannot connect to the API server")
            finalCoords = {
                "WSG84": {
                    "lat": 0,
                    "lng": 0
                },
                "EPSG2326": {
                    "easting": coords[0],
                    "northing": coords[1]
                }
            }

    print(finalCoords)

    return finalCoords
