import os
import glob
import json
import datetime
import re
from tkinter.tix import Tree
from numpy import ERR_LOG
import requests
from lxml import etree


from components.io import requests_from_url
from components.io import convert_XML2JSON

from components.string_process import clean_newline
from components.string_process import remove_whitespace
from components.string_process import address_tolower
from components.string_process import str2int
from components.string_process import add_comma_behind_str

from components.hashing import crc32


def download_files():
    # download the file into memory
    res = requests.get(
        "https://opendata.clp.com.hk/GetChargingSectionXML.aspx?lang=EN", headers=headers)

    # write xml file into the HDD
    myfile = open("EV_raw.xml", "w")
    myfile.write(res.text)

    return res


def process_string(str):
    return remove_whitespace(clean_newline(str, " "))


def vehicle_supported_type(s):
    if s == None:
        return "General"
    elif s.lower() == "Tesla only".lower():
        return "Tesla"
    elif s.lower() == "BYD only".lower():
        return "BYD"
    else:
        return "General"


def check_public_permit(s):
    if s == None:
        return True
    elif "permit" in s:
        return False
    else:
        return True


def create_rangeList(start, end):
    rL = []

    for i in range(start, end + 1):
        rL.append(i)

    return rL


def turn_parking_slot_into_list(s):

    regexNumber = ".*?([0-9]+)[\n]*"  # only the number at the end
    regexPrefix = "(.*?)[0-9]+[\n]*"

    # s = "D042 - D052, D106 - D112"
    if s == None:
        return ["No information provided"]
    elif s.lower() == "Tesla only".lower() or s == "BYD only".lower() or "permit" in s or s == "":
        return ["No information provided"]
    else:
        parking_slot_list = []

        raw_comma_split = s.split(",")
        # ["D042 - D052", "D106 - D112", D16]

        for ele in raw_comma_split:
            ele = process_string(ele)

            isRange = False

            if "-" in ele:  # D042 - D052
                isRange = True

            if not isRange:
                parking_slot_list.append(ele)

            if isRange:
                ele_dash_split = ele.split("-")

                for obj in ele_dash_split:
                    obj = process_string(obj)

                #D042 is ele_dash_split[0]
                item0_number = re.findall(
                    regexNumber, ele_dash_split[0])[-1]  # 042
                item0_prefix = ele_dash_split[0][0:ele_dash_split[0].find(
                    item0_number)]  # D0

                #D052 is ele_dash_split[-1]
                item1_number = re.findall(
                    regexNumber, ele_dash_split[-1])[-1]  # 042

                item1_prefix = ele_dash_split[-1][0:
                                                  ele_dash_split[-1].find(
                                                      item0_number)
                                                  ]  # D0

                rangeList = create_rangeList(
                    int(item0_number), int(item1_number))
                #42, 43, 44

                for parkingNo in rangeList:
                    parking_slot_list.append(item0_prefix + str(parkingNo))
                    #D042, D043, D044

    return parking_slot_list


def create_string_for_checksum(location, parkingNo):

    if location == None:
        location = "None"

    if parkingNo == None:
        parkingNo = "None"

    return location + "_" + parkingNo


def process_data(data):
    """
        "vehicle" value should be Tesla, BYD or General
    """
    newStationsList = []
    counter = 0

    data2process = data["ChargingStationData"]["stationList"]["station"][0:50]

    # print(data2process)

    for station in data2process:

        newStationData = {
            "uuid": station["no"],
            "address": {
                "full": {
                    "zh": "",
                    "en": address_tolower(process_string(station["address"])).title()
                },
                "streetName": add_comma_behind_str(process_string(station["address"]), "carpark"),
                "region": process_string(station["districtL"]),
                "district": process_string(station["districtS"]),
                "locationName": {
                    "zh": "",
                    "en": process_string(station["location"]),
                },
                "geocode": {
                    "WGS84": {
                        "lat": str2int(process_string(station["lat"])),
                        "lng": str2int(process_string(station["lng"]))
                    }
                }
            },
            "provider": process_string(station["provider"]),
            "type": {
                "charging": process_string(station["type"]).split(";"),
                "vehicle": vehicle_supported_type(station["parkingNo"])
            },
            "publicPermit": check_public_permit(station["provider"]),
            "parkingSlot": turn_parking_slot_into_list(station["parkingNo"]),
            "updateCheckSum": crc32(create_string_for_checksum(station["location"], station["parkingNo"]))
        }

        # print(newStationData)

        newStationsList.append(newStationData)
        counter += 1

        print("processing {} of {}".format(
            counter, len(data2process)), end="\r", flush=True)

        if counter % 20 == 0:
            x = open("../data/cleaned_EV_raw.json", "w")
            x.write(json.dumps(newStationsList, indent=4))
            x.close()


def main():
    startTime = datetime.datetime.now()

    currentDir = os.getcwd()
    XMLfolder = "data"

    rawFileName = "EV_raw.xml"

    fullXMLPath = os.path.join(os.path.join(
        currentDir, XMLfolder), rawFileName)

    # print(fullXMLPath)

    listofFile = glob.glob(os.path.join("../data", rawFileName))

    # print(listofFile)

    if len(listofFile) == 0:
        rawFileExist = False
    else:
        if rawFileName in listofFile[-1]:
            rawFileExist = True
        else:
            rawFileExist = False

    if not rawFileExist:
        print("Raw XML file is not exist, download the file throught the API ...\n")
        requests_from_url(
            "https://opendata.clp.com.hk/GetChargingSectionXML.aspx?lang=EN", "../data/"+rawFileName, "XML")

        # convert the xml to json
        root = etree.parse(r"../data/EV_raw.XML")
        print("root: {}".format(root))
        xml = etree.tostring(root)
        data = convert_XML2JSON(xml, "../data/EV_raw.json")
    else:
        print("raw file exist, will read the file now")

    x = open("../data/EV_raw.json")
    data = json.load(x)

    # print(data)

    process_data(data)

    timeNow = datetime.datetime.now()
    print(
        "Total use {} seconds to run".format(
            round((timeNow - startTime).total_seconds()))
    )


if __name__ == "__main__":
    main()
